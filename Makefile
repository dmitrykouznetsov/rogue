CC = gcc
CFLAGS = -Wall -lncurses -I$(IDIR)

IDIR = ./include/
SRCDIR = ./src/

SOURCES = $(SRCDIR)*.c

all: rogue run clean

rogue:
	$(CC) $(SOURCES) $(CFLAGS) -o $@.o

run:
	./rogue.o

clean:
	rm rogue.o
