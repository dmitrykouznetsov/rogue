#ifndef ROGUE_H
#define ROGUE_H

#include <ncurses.h>
#include <stdlib.h>


typedef struct Coordinate
{
    int x;
    int y;
} Coordinate;

typedef struct Room
{
    Coordinate coordinate;
    int width;
    int height;
    // Item ** items;
} Room;

typedef struct Character
{
    Coordinate coordinate;
} Character;

// General drawing
int setupScreen();

// Types of objects
Room ** setupMap();
Character * setupCharacter();

// Character functions
int handleInput(int input, Character * you);
int characterMove(Coordinate coord, Character * you);
int checkPosition(Coordinate coord, Character * c);

// Room functions
Room * createRoom(int y, int x, int height, int width);
int drawRoom(Room * room);

#endif
