#include <rogue.h>


/*
 * Place the character at an initial position in the game world
 */
Character * setupCharacter()
{
        Character * newCharacter;
        newCharacter = malloc(sizeof(Character));

        Coordinate initial_position;
        initial_position.x = 14;
        initial_position.y = 14;

        characterMove(initial_position, newCharacter);
        return newCharacter;
}

/*
 * Enable movement of the character
 */
int handleInput(int input, Character * you)
{
        Coordinate new_position;
        // Use WASD keys for movement
        switch (input) {
                case 'w':
                        new_position.y = you->coordinate.y - 1;
                        new_position.x = you->coordinate.x;
                        break;

                case 'a':
                        new_position.y = you->coordinate.y;
                        new_position.x = you->coordinate.x - 1;
                        break;

                case 's':
                        new_position.y = you->coordinate.y + 1;
                        new_position.x = you->coordinate.x;
                        break;

                case 'd':
                        new_position.y = you->coordinate.y;
                        new_position.x = you->coordinate.x + 1;
                        break;

        }

        checkPosition(new_position, you);
        return 1;
}

/*
 * Check what is at the future position
 */
int checkPosition(Coordinate coord, Character * c)
{
        switch (mvinch(coord.y, coord.x)) {
                case ' ':
                        characterMove(coord, c);
                        break;

                default:
                        move(c->coordinate.y, c->coordinate.x);
                        break;
        }

        return 1;
}

/*
 * Update character position
 */
int characterMove(Coordinate coord, Character * you)
{
        // First replace the character tile by an empty tile
        mvprintw(you->coordinate.y, you->coordinate.x, " ");

        you->coordinate = coord;

        // Redraw the user in the updated position
        mvprintw(you->coordinate.y, you->coordinate.x, "@");
        move(you->coordinate.y, you->coordinate.x);
        return 1;
}
