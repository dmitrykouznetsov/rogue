#include <rogue.h>


int main ()
{
        Character * you;
        int ch;
        setupScreen();

        setupMap();

        you = setupCharacter();

        // Start the game loop here
        while ((ch = getch()) != 'q')
                handleInput(ch, you);

        endwin();

        return 0;
}

int setupScreen() {
        initscr();

        if (has_colors() == FALSE) {
                endwin();
                printf("Your terminal does not support color\n");
                exit(1);
        }

        // start_color();
        // use_default_colors();

        // We don't want to see what is being typed
        cbreak();
        noecho();
        refresh();

        return 1;
}
