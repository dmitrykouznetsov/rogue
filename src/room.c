#include <rogue.h>


/*
 * Draw the rooms
 */
Room ** setupMap()
{
        Room ** rooms;
        rooms = malloc(sizeof(Room) * 6);

        rooms[0] = createRoom(13, 13, 6, 8);
        rooms[1] = createRoom(10, 50, 10, 20);
        rooms[2] = createRoom(4, 25, 5, 10);

        drawRoom(rooms[0]);
        drawRoom(rooms[1]);
        drawRoom(rooms[2]);

        return rooms;
}


Room * createRoom(int y, int x, int height, int width)
{
        Room * newRoom;
        newRoom = malloc(sizeof(Room));

        Coordinate position;
        position.y = y;
        position.x = x;
        newRoom->coordinate = position;
        newRoom->width = width;
        newRoom->height = height;

        return newRoom;
}

/*
 * Draw a single room
 *
 * When drawing the rooms modifiers are applied such that the
 * dimensions of the rooms denote the available area the character
 * can use to move around.
 */
int drawRoom(Room * room)
{
        int x, y;

        // Draw top and bottom
        for (x = room->coordinate.x; x < room->coordinate.x + room->width + 2; x++) {
                mvprintw(room->coordinate.y, x, "-");
                mvprintw(room->coordinate.y + room->height + 1, x, "-");
        }

        // Draw the sidewalls
        for (y = room->coordinate.y + 1; y < room->coordinate.y + room->height + 1; y++) {
                mvprintw(y, room->coordinate.x, "|");
                mvprintw(y, room->coordinate.x + room->width + 1, "|");
        }

        return 1;
}
